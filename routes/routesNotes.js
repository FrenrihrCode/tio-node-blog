const express = require('express');
const Noticias = require('../models/noticias');
const Usuario = require('../models/usuario');
const {isAuthenticated} = require('../middleware/autenticacion');
const router = express.Router();

router.get("/blog", async function (req, res) {
    const noticias = await Noticias.find({});
    res.render('blog', { noticias })
});
router.post('/blog_crear', isAuthenticated,  async function(req, res) {
    const { titulo, encabezado, descripcion, image, autor, categoria, tag, tag1, tag2 } = req.body;
    let tags = [];
    tags.push(tag);
    if(tag1) tags.push(tag1)
    if(tag2) tags.push(tag2)
    let newNota = new Noticias({ titulo, encabezado, descripcion, image, autor, categoria, tags });
    await newNota.save(function(err){
        if( err ) console.log(err);
    });
    res.redirect("/blog");
});
router.get("/blog/:id", async function (req, res) {
    const noticia = await Noticias.findById(req.params.id);
    res.render('blog-single', {noticia: noticia})
});
router.get("/blog/categoria/:categoria", async function (req, res) {
    const categoria = req.params.categoria
    const noticias = await Noticias.find({categoria:req.params.categoria});
    res.render('blog', {noticias: noticias, categoria:categoria})
});
router.post('/blog/:id/comentar', isAuthenticated, async function(req, res) {
    const { comentario } = req.body;
    let img_usuario = 'usuario.png';
    let nombre_usuario = req.user.nombre;
    const noticia = await Noticias.findById(req.params.id);
    noticia.comentarios.push({ nombre_usuario, img_usuario, comentario });
    await noticia.save(function(err){
        if( err ) console.log(err);
    });
    res.redirect("/blog/" + req.params.id);
});
router.get('/editBlog/:id', isAuthenticated, async function(req, res){
    let error = [];
    const usersName = await Usuario.find({});
    const noticia = await Noticias.findById(req.params.id);
    res.render('editBlog', {error, noticia, usersName })
});
router.put('/updateblog/:id', isAuthenticated, async function(req, res){
    const { titulo,  encabezado, descripcion,image,autor,categoria,tag,tag1,tag2 } = req.body;
    let tags = [];
    tags.push(tag);
    if(tag1) tags.push(tag1)
    if(tag2) tags.push(tag2)
    await Noticias.findByIdAndUpdate(req.params.id, { titulo: titulo,encabezado:encabezado, descripcion: descripcion, image: image,autor:autor,categoria:categoria,tags:tags });
    res.redirect("/blog");
})
router.delete('/deleteBlog/:id', isAuthenticated, async function(req, res) {
    let id = req.params.id;
    await Noticias.findByIdAndDelete(id);
    res.redirect("/blog");
});

module.exports = router