const express = require('express');
const Usuario = require('../models/usuario');
const Contacto = require('../models/contacto');
const router = express.Router();
const passport = require('passport');
const {isAuthenticated} = require('../middleware/autenticacion');

router.get('/login', function(req, res){
    let error = [];
    if(req.session.messages) error.push({text: req.session.messages})
    console.log(error)
    res.render('login', {error, 
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});
router.get('/register', function(req, res){
    let error = [];
    res.render('register', {error, 
        username: '',
        email: '',
        password: '',
        confirmPassword: ''})
});
router.get('/welcome',isAuthenticated, async function(req, res){
    const contactos = await Contacto.find({});
    res.render('welcome', {contactos})
});
router.delete('/deleteComment/:id',isAuthenticated, async function(req, res) {
    let id = req.params.id;
    await Contacto.findByIdAndDelete(id);
    res.redirect("/welcome");
});
router.post('/registrar',  async function(req, res) {
    let error = [];
    const { username, email, password, confirmPassword } = req.body;
    if (password != confirmPassword) {
        error.push({ text: "Contraseñas no coinciden." });
        res.render("register", {
            error,
            username,
            email,
            password,
            confirmPassword
        });
    } else {
        // Look for email coincidence
        const emailUser = await Usuario.findOne({ email: email });
        const nameUser = await Usuario.findOne({ nombre: username });
        if (emailUser) {
            error.push({ text: "El correo ya esta en uso." });
        }
        if (nameUser) {
            error.push({ text: "El usuario ya esta en uso." });
        }
        if(error.length >0 ){
            res.render("register", {
                error,
                username,
                email,
                password,
                confirmPassword
            });
        } else {
          // Saving a New User
          const newUser = new Usuario({ nombre:username, email:email, password:password });
          newUser.password = await newUser.encryptPassword(password);
          await newUser.save();
          res.redirect("/welcome");
        }
    }
    
});
router.post('/ingresar', passport.authenticate("local", {
    successRedirect: "/welcome",
    failureRedirect: "/login",
    failureMessage: 'Credenciales inválidas'
  }));
    /*
    let error = [];
    const { email, password } = req.body;
    const user = await Usuario.findOne({email: email});
    if (!user) {
        error.push({ text: "Credenciales inválidas." });
        res.render("login", {
            error,
            email,
            password
        });
    } else {
        const match = await user.matchPassword(password);
        if(match) {
            res.redirect("/welcome");
        } else {
            error.push({ text: "Credenciales inválidas." });
            res.render("login", {
                error,
                email,
                password
            });
        }
        
    }
});*/
router.get('/salir', (req, res) => {
    req.logout();
    res.redirect("/");
  });
module.exports = router