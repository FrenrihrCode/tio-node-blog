const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const ContactoSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    telf: {
      type: String,
      required: true
    },
    date: {
      type: String,
      required: true
    },
    message: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Contacto", ContactoSchema);